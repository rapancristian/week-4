#pragma once

#include "catchorg/catch/catch.hpp"
#include "summer_school/raytracing/color.hpp"

using summer_school::raytracing::color::Color;
using summer_school::raytracing::color::Colors;

TEST_CASE("Colors")
{

	SECTION("Default Constructor")
	{
		Color myColor(0x93F542FF);
		REQUIRE(myColor.getRed() == 0x93);
		REQUIRE(myColor.getGreen() == 0xF5);
		REQUIRE(myColor.getBlue() == 0x42);
		REQUIRE(myColor.getAlpha() == 0xFF);
	}

	SECTION("New Constructor")
	{
		Color myColor(Colors::BrightGreen);
		REQUIRE(myColor.getRed() == 0x93);
		REQUIRE(myColor.getGreen() == 0xF5);
		REQUIRE(myColor.getBlue() == 0x42);
		REQUIRE(myColor.getAlpha() == 0xFF);
	}

	SECTION("Get code")
	{
		Color myColor(Colors::BrightGreen);
		REQUIRE(myColor.getCode() == 0x93F542FF);
	}




}