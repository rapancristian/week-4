#include "summer_school/raytracing/bitmap.hpp"
#include "summer_school/raytracing/color.hpp"
#include "summer_school/raytracing/utility.hpp"

#include <algorithm>
#include <climits>
#include <cstdint>
#include <fstream>

using summer_school::raytracing::color::Color;
using summer_school::raytracing::image::Image;
using channel_type = uint8_t;

namespace summer_school::raytracing::bitmap {

	static constexpr channel_type magicWords[] = { 0x42, 0x4D };
	static constexpr auto bitsPerPixel = 24;
	static constexpr auto horizontalResolution = 2835;
	static constexpr auto verticalResolution = 2835;
	static constexpr auto pixelArrayOffset = 54;
	static constexpr auto pixelArrayPadding = 4;
	static constexpr auto dibHeaderSize = 40;
	static constexpr auto colorPlanes = 1;

	template<int byteCount, typename Address>
	auto set(Address& address, uint32_t value) {
		static_assert(0 < byteCount || byteCount <= 4);
		if constexpr (byteCount >= 1)
			address[0] = value & 0xFF;
		if constexpr (byteCount >= 2)
			address[1] = (value >> 8) & 0xFF;
		if constexpr (byteCount >= 4) {
			address[2] = (value >> 16) & 0xFF;
			address[3] = (value >> 24) & 0xFF;
		}
		address += byteCount;
	}

	static auto bitmapFileBinary(const Image& image) {
		auto bitmap = std::vector<channel_type>(pixelArrayOffset, 0);
		for (auto y = 0; y < image.getHeight(); ++y) {
			for (auto x = 0; x < image.getWidth(); ++x) {
				auto color = image.getColor(x, y).getCode();
				bitmap.push_back((color >> 8) & 0xFF);
				bitmap.push_back((color >> 16) & 0xFF);
				bitmap.push_back((color >> 24) & 0xFF);
			}
			while ((bitmap.size() - pixelArrayOffset) % pixelArrayPadding)
				bitmap.push_back(0);
		}
		auto address = bitmap.begin();
		/*0x00*/ set<1>(address, magicWords[0]);
		/*0x01*/ set<1>(address, magicWords[1]);
		/*0x02*/ set<4>(address, bitmap.size());
		/*0x06*/ set<4>(address, 0);
		/*0x0A*/ set<4>(address, pixelArrayOffset);
		/*0x0E*/ set<4>(address, dibHeaderSize);
		/*0x12*/ set<4>(address, image.getWidth());
		/*0x16*/ set<4>(address, image.getHeight());
		/*0x1A*/ set<2>(address, colorPlanes);
		/*0x1C*/ set<2>(address, bitsPerPixel);
		/*0x1E*/ set<4>(address, 0);
		/*0x22*/ set<4>(address, bitmap.size() - pixelArrayOffset);
		/*0x26*/ set<4>(address, horizontalResolution);
		/*0x2A*/ set<4>(address, verticalResolution);
		/*0x2E*/ set<4>(address, 0);
		/*0x32*/ set<4>(address, 0);
		return bitmap;
	}

	auto writeBitmapFile(const std::string& path, const Image& image) -> void {
		auto file = std::ofstream{ path, std::ios_base::binary };
		file << std::noskipws;
		file.exceptions(std::ofstream::failbit | std::ofstream::badbit);
		auto bitmap = bitmapFileBinary(image);
		for (auto byte : bitmap)
			if (file)
				file << byte;
			else
				THROW(std::runtime_error, "stream corruption while writing the bitmap file");
		file.close();
	}

	static auto getDoubleWord(channel_type* bytes) {
		uint32_t value = bytes[3];
		(value <<= 8) |= bytes[2];
		(value <<= 8) |= bytes[1];
		(value <<= 8) |= bytes[0];
		return value;
	}

	auto readBitmapFile(const std::string& path) -> Image {
		auto file = std::ifstream{ path, std::ios_base::binary };
		file >> std::noskipws;

		channel_type buffer[32];
		auto readBytes = [&file, &buffer](int count) mutable {
			return static_cast<bool>(file.read(reinterpret_cast<char*>(buffer), count));
		};

		if (!file)
			THROW(std::runtime_error, "file '", path, "' does not exist");

		if (!readBytes(2) || buffer[0] != magicWords[0] || buffer[1] != magicWords[1])
			THROW(std::runtime_error, "invalid magic words in header -- ", magicWords[0], " ", magicWords[1],
				" were expected instead of ", static_cast<int>(buffer[0]), " ", static_cast<int>(buffer[1]));

		if (!readBytes(16)) // skip to width at offset 18
			THROW(std::runtime_error, "bytes 2-17 are missing from the header");

		if (!readBytes(4)) // read width
			THROW(std::runtime_error, "couldn't read width from the header");
		auto readWidth = getDoubleWord(buffer);
		if (readWidth > INT_MAX)
			THROW(std::runtime_error, "width is too large");
		auto width = static_cast<int>(readWidth);

		if (!readBytes(4)) // read height
			THROW(std::runtime_error, "couldn't read height from the header");
		auto readHeight = getDoubleWord(buffer);
		if (readHeight > INT_MAX)
			THROW(std::runtime_error, "height is too large");
		auto height = static_cast<int>(readHeight);

		if (!readBytes(28)) // skip to pixel array at offset 54
			THROW(std::runtime_error, "bytes 26-53 are missing from the header");

		auto image = Image{ width, height };
		auto pixelArraySize = 0;
		for (auto i = 0; i < height; i++) {
			for (auto j = 0; j < width; j++) {
				if (!readBytes(3))
					THROW(std::runtime_error, "color information is missing from the pixel array");
				uint32_t colorCode = buffer[2];
				(colorCode <<= 8) |= buffer[1];
				(colorCode <<= 8) |= buffer[0];
				(colorCode <<= 8) |= 0xFF;
				image.plot(j, i, Color{ colorCode });
				pixelArraySize += 3;
			}
			auto padding = (pixelArraySize % 4) ? 4 - (pixelArraySize % 4) : 0;
			pixelArraySize += padding;
			if (!readBytes(padding))
				THROW(std::runtime_error, "some padding bytes are missing from the pixel array");
		}
		return image;
	}

}
