#pragma once
#include "catchorg/catch/catch.hpp"
#include "summer_school/raytracing/image.hpp"

using summer_school::raytracing::image::Image;
using summer_school::raytracing::color::Colors;

TEST_CASE("image")
{
    SECTION("image constructor")
    {
        auto image = Image(10, 10);

		SECTION("default color")
		{
			REQUIRE(image.getColor(5, 5) == Colors::Black);
		}

		SECTION("plot")
		{
			image.plot(5, 5, Colors::Red);
			REQUIRE(image.getColor(5, 5) == Colors::Red);
		}

		SECTION("throw exeption")
		{
			REQUIRE_THROWS_AS(image.plot(1, 15, Colors::Black), std::out_of_range);
			REQUIRE_THROWS_AS(image.getColor(10, 15), std::out_of_range);
		}
    }
}