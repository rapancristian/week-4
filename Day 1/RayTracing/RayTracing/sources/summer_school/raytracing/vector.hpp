#pragma once
#include <limits>
namespace summer_school::raytracing::vector
{
	class Vector3
	{
	public:
		constexpr Vector3() : x_(0), y_(0), z_(0)
		{
			// Empty
		}
		constexpr Vector3(double x, double y, double z) : x_(x), y_(y), z_(z)
		{
			// Empty
		}

		constexpr Vector3(const Vector3&) = default;

		constexpr  Vector3(Vector3&&) = default;

		constexpr Vector3& operator=(const Vector3&) = default;

		constexpr Vector3& operator=(Vector3&&) = default;

		constexpr auto getX() const { return x_; }
		constexpr auto getY() const { return y_; }
		constexpr auto getZ() const { return z_; }

		auto getMagnitude() { return std::sqrt(x_ * x_ + y_ * y_ + z_ * z_);}

		// Inplace Addition
		constexpr Vector3& operator+=(const Vector3& other)
		{
			this->x_ += other.x_;
			this->y_ += other.y_;
			this->z_ += other.z_;
			return *this;
		}

		// Inplace Subtraction
		constexpr Vector3& operator-=(const Vector3& other)
		{
			this->x_ -= other.x_;
			this->y_ -= other.y_;
			this->z_ -= other.z_;
			return *this;
		}

		constexpr Vector3& operator*=(double scalar)
		{
			this->x_ *= scalar;
			this->y_ *= scalar;
			this->z_ *= scalar;
			return *this;
		}


		friend constexpr Vector3& operator+ (Vector3 first, const Vector3& second)
		{
			return first += second;
		}

		friend constexpr Vector3& operator- (Vector3 first, const Vector3& second)
		{
			return first -= second;
		}

		friend constexpr Vector3& operator* (Vector3 vector3, double scalar)
		{
			return vector3 *= scalar;
		}

		friend constexpr Vector3& operator* (double scalar, Vector3 vector3)
		{
			return vector3 *= scalar;
		}

		friend bool operator == (const Vector3& first, const Vector3& second)
		{
			auto x = first.getX() - second.getX();
			auto y = first.getY() - second.getY();
			auto z = first.getZ() - second.getZ();

			return x * x + y * y + z * z < std::numeric_limits<double>::epsilon() * std::numeric_limits<double>::epsilon();
		}


	private:
		double x_;
		double y_;
		double z_;
	};

	constexpr auto dotProduct(const Vector3& first, const Vector3& second)
	{
		return first.getX() * second.getX() + first.getY() * second.getY() + first.getZ() * second.getZ();
	}


	constexpr auto close(const Vector3& first, const Vector3& second, double epsilon = 1e-15)
	{
		auto third = first - second;

		return dotProduct(third, third) < epsilon * epsilon;
	}
	
	inline auto normalized(Vector3 vector3)
	{
		return vector3 *= (1.0 / vector3.getMagnitude());
	}

}

/*
// Not working tried
friend bool operator == (const Vector3& first, const Vector3& second)
{
	double xDifference = std::abs(first.getX() - second.getX());
	double yDifference = std::abs(first.getY() - second.getY());
	double zDifference = std::abs(first.getZ() - second.getZ());

	bool areClose = (xDifference < std::numeric_limits<double>::epsilon() && yDifference < std::numeric_limits<double>::epsilon() && zDifference < std::numeric_limits<double>::epsilon());
	return areClose;
}
*/


/*
// USUAL WAY
friend constexpr Vector3& operator+ (const Vector3& first, const Vector3& second)
{
	auto copy = first;
	return copy += second;
}
*/

/*
// Old close
constexpr auto close(const Vector3& first, const Vector3& second, double epsilon)
{
	auto x = first.getX() - second.getX();
	auto y = first.getY() - second.getY();
	auto z = first.getZ() - second.getZ();

	return x * x + y * y + z * z < epsilon * epsilon;
}
*/